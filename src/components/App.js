import React, {Component} from 'react';
import Paper from './paper/Paper';
import EditTool from './paper/edit-tool/EditTool';
import {user} from './test';

import './App.css';

class App extends Component {

    state = {
        paperIds: user.id[29].paperIds
    };

    render() {
        const {paperIds} = this.state;
        const papers = paperIds.map((paperId) => <Paper id={paperId} key={paperId}/>);

        return (
            <div>
                {papers}
                <EditTool/>
            </div>
        );
    }
}


export default App;
