import React, {Component} from 'react';
import ParagraphList from './paragraph-list/ParagraphList';
import {paper} from '../test';

import './Paper.scss'

export default class Paper extends Component {
    state = {
        id: 0,
        paragraphListIds: []
    };

    static getDerivedStateFromProps(props, state) {
        return {
            id: props.id,
            paragraphListIds: paper.id[props.id].paragraphListIds
        }
    }

    render() {
        const {paragraphListIds} = this.state;
        const paragraphLists = paragraphListIds.map((paragraphListId) => <ParagraphList key={paragraphListId} id={paragraphListId}/>);
        return (
            <div className='paper'>
                {paragraphLists}
            </div>
        );
    }
}
