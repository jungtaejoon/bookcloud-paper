import React, {Component} from 'react';
import Paragraph from './paragraph/Paragraph';
import {paragraphList} from "../../test";

import './ParagraphList.scss'

class ParagraphList extends Component {
    state = {
        id: 0,
        paragraphIds: []
    };

    static getDerivedStateFromProps(props, state) {
        return {
            id: props.id,
            paragraphIds: paragraphList.id[props.id].paragraphIds
        }
    }

    render() {
        const {paragraphIds} = this.state;
        const paragraphs = paragraphIds.map((paragraphId, i) => <Paragraph key={paragraphId} id={paragraphId} left={i * 40}/>);
        return (
            <div className='paragraph-list'>
                {paragraphs}
            </div>
        );
    }
}

export default ParagraphList;