import React, {Component} from 'react';
import {paragraph} from '../../../test';

import './Paragraph.scss'

class Paragraph extends Component {
    state = {
        id: 0,
        text: ''
    };

    static getDerivedStateFromProps(props, state) {
        return {
            id: props.id,
            text: paragraph.id[props.id].text,
            tags: ['tag', '태그'],
            left: props.left
        }
    }

    render() {
        const {text, tags, left} = this.state;
        const style = {
            left: left + 'px'
        };
        return (
            <div className='paragraph' style={style}>

                <div className='card-content'>
                    <div className='text'>
                        <div className='title'>
                            {'test'}
                        </div>
                        <div className='description'>
                            {text}
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Paragraph;