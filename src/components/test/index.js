const data = {
    user: {
        id: {
            29: {
                paperIds: [28, 39]
            }
        },
    },
    paper: {
        id: {
            28: {
                paragraphListIds: [39, 48]
            },
            39: {
                paragraphListIds: [2, 35]
            }
        }
    },
    paragraphList: {
        id: {
            39: {
                paragraphIds: [38, 39, 3, 12, 34, 33, 8]

            },
            48: {
                paragraphIds: [3]

            },
            2: {
                paragraphIds: [12, 34]

            },
            35: {
                paragraphIds: [33, 8]

            }
        }
    },
    paragraph: {
        id: {
            38: {
                text: '동해물과 백두산이 마르고 닳도록'

            },
            39: {
                text: '하느님이 보우하사 우리나라 만세'

            },
            3: {
                text: '무궁화 삼천리 화려강산'

            },
            12: {
                text: '대한사람 대한으로 길이 보전하세'

            },
            34: {
                text: '남산 위에 저 소나무 철갑을 두른 듯'

            },
            33: {
                text: '바람서리 불변함은 우리 기상일세'

            },
            8: {
                text: '가을 하늘 공활한데 높고 구름 없이'

            }
        }
    }
};

export const {user, paper, paragraphList, paragraph} = data;